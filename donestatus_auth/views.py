from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from django.db import IntegrityError
from rest_framework.decorators import api_view, permission_classes
from .serializers import UserSerializer, UserAuthTokenSerializer
from authentication.models import AppUser

class UserViewSet(viewsets.ModelViewSet):
    queryset = AppUser.objects.all()
    serializer_class = UserSerializer

# Create your views here.

# @api_view(["POST", ])
# @permission_classes([AllowAny, ])
# def signup_user(request):
# 	try:
# 		user_input = request.data
# 		new_user = AppUser.objects.create_user(
# 			email=user_input.get('email'),
# 			username=user_input.get('username'),
# 			password=user_input.get('password')
# 		)
# 		return Response(UserAuthTokenSerializer(new_user, many=False).data,200)
# 	except IntegrityError:
# 		return Response("User with similar username already exists")
# 	except Exception as ex:
# 		return Response(str(ex), 400)


# @api_view(['POST', ])
# @permission_classes([AllowAny, ])
# def login_user(request):
# 	try:
# 		data = request.data
# 		user = authenticate(username=data.get('username'),
# 							password=data.get('password'))

# 		if not data.get('remember'):
# 			request.session.set_expiry(0)
# 		if user is None:
# 			return Response({'status':'error',
# 							 'detail': 'Invalid username and password',
# 							 }, 400)

# 		if not user.is_active:
# 			return Response({'status':'error',
# 							 'detail': 'Your account has been disabled',
# 							 }, 400)

# 		return Response(UserAuthTokenSerializer(user, many=False).data)

# 	except Exception as e:
# 		return Response({'status':'error',
# 							 'detail': str(e),
# 							 }, 400)

