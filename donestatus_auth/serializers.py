from django.contrib.auth.models import User
from rest_framework import serializers
from authentication.models import AppUser

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = AppUser
        fields = ('url', 'username', 'email', 'is_staff')


class UserAuthTokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppUser
        fields = ('id', 'auth_token',)