from .base import *


# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'donestatus',
        'USER': 'donestatus',
        'PASSWORD': 'donestatus',
        'HOST': '127.0.0.1',
        'PORT': '5432', 
    }
}


try:
	from .local import *
except ImportError:
	pass