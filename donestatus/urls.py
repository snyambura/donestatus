
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets
from status import views as update_views
from status import urls as status_urls
from donestatus_auth import views as auth_views
from django.views.generic import TemplateView, RedirectView
from status import views as update_views
from authentication import urls as authentication_urls
from authentication import views as authentication_views
import views as base_views




# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', auth_views.UserViewSet)
# router.register(r'updates', update_views.YesterdayViewSet)
# router.register(r'updates', update_views.TodayViewSet)
# router.register(r'updates', update_views.ChallengesViewSet)



urlpatterns = [
    # url(r'^$', TemplateView.as_view(template_name="home.html"), name='home'),
    url(r'^api/', include(router.urls)),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^status/', include(status_urls)),
    url(r'^accounts/login/$', authentication_views.log_in, name='login'),
    url(r'^register/$', authentication_views.register, name='register'),
    url(r'^logout/$', authentication_views.log_out, name='logout'),
    url(r'^$', base_views.home, name='home'),
    url(r'^login/$', authentication_views.log_in, name='login'),


 
]
