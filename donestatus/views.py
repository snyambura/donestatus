
from datetime import datetime, timedelta
from django.shortcuts import render
# from main.apps import dashboard
from authentication.forms import RegistrationForm
from main.forms import FilterForm
from taggit.models import Tag, TaggedItem
from status.models import Updates
from django.db.models import Q
from datetime import datetime

def home(request):
	"""Some default page for the home page / Dashboard."""
	
	try:
		my_dates = []
		templ = ''
		form = RegistrationForm()
		form2= FilterForm()
		status_updates=[]
		# dash = dashboard()

		if request.user.is_authenticated():
			filters = Q(is_void=False)
			if request.method == 'POST':
				owner = request.user
				query_tags = request.POST.get('tags')
				task_date = request.POST.get('task_date')
				person= request.POST.get('person')
				form2= FilterForm(initial={'tags': query_tags, 'task_date': task_date, 'person':person})

				if query_tags:
					tag= Tag.objects.get(id=query_tags)
					if tag:
						filters &= Q(tags__name__in=[str(tag.name),])

				if person:
					filters &= Q(owner= person)
				
				if task_date:
					task_date_obj = datetime.strptime(task_date, '%Y-%m-%d')
					filters &= Q(created_at__year=task_date_obj.year, created_at__month=task_date_obj.month, created_at__day=task_date_obj.day)

				status_updates = Updates.objects.filter(filters).distinct().order_by('-created_at')
			else:
				status_updates = Updates.objects.all().order_by('-created_at')

			templ= 'dashboard.html'
		else:
			templ = 'home.html'
			
		return render(request, templ,
				  {'status': 200, 'form': form, 'status_updates': status_updates, 'form2':form2 })
	except Exception, e:
		raise e



