from __future__ import unicode_literals

from django.db import models
from taggit.managers import TaggableManager

class Updates(models.Model):
    workon = models.TextField()
    update = models.TextField()
    challenges = models.TextField()
    tags = TaggableManager()
    owner = models.ForeignKey('authentication.AppUser')
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)
    is_void = models.BooleanField(default=False)

    class Meta:
		db_table= 'updates'




