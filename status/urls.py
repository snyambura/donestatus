"""URLs for status module."""
from django.conf.urls import url
from .views import status_update, view_history

# This should contain urls related to status app ONLY
urlpatterns = [
               url(r'^status_update/$',status_update, name='status_update'),
               url(r'^view_history/$',view_history, name='view_history'),

               ]
