from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, BasePermission
from authentication.models import AppUser
from django.shortcuts import render
# from .serializers import YesterdaySerializer, TodaySerializer, ChallengesSerializer
from .models import Updates
from django.shortcuts import get_object_or_404
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.decorators import api_view, permission_classes
from .forms import UpdateStatusForm
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from donestatus.views import home
from django.utils import timezone


# class YesterdayViewSet(viewsets.ModelViewSet):
# 	"""
# 	Team status updates
# 	"""
# 	queryset = Yesterday.objects.all()
# 	serializer_class = YesterdaySerializer

# class TodayViewSet(viewsets.ModelViewSet):
# 	"""
# 	Team status updates
# 	"""
# 	queryset = Today.objects.all()
# 	serializer_class = TodaySerializer

# class ChallengesViewSet(viewsets.ModelViewSet):
# 	"""
# 	Team status updates
# 	"""
# 	queryset = Challenges.objects.all()
# 	serializer_class = ChallengesSerializer


def status_update(request):
	try:
		form = UpdateStatusForm(request.POST or None)
		if form.is_valid():
			# Get app_user
			owner = request.user

			update = Updates.objects.create(
				update=form.cleaned_data['update'],
				workon=form.cleaned_data['workon'],
				challenges=form.cleaned_data['challenge'],
				owner=owner 
			)

			#add tags
			tagss = form.cleaned_data['tags']
			for tag in tagss:
				update.tags.add(tag)
				


			msg = 'saved successfully.'
			messages.add_message(request, messages.INFO, msg)
			redirect_url = reverse(home)
			return HttpResponseRedirect(redirect_url)
	except Exception, e:
		print 'An error occured while saving - %s' % str(e)
		msg = 'Error '
		messages.add_message(request, messages.ERROR, msg)
		redirect_url = reverse(status_update)

	return render(request, 'status/status_update.html', {'form': form})


def view_history(request):
	try:
		username = request.user.get_username()
		app_user = AppUser.objects.get(username=username)

		results = Updates.objects.filter(owner=app_user)

		return render(request,
					  'status/view_history.html',
					  {
					  'results': results,
					   })
	except Exception, e:
		msg = 'An error occured trying to view Task History - %s' % (str(e))
		messages.add_message(request, messages.ERROR, msg)
	redirect_url = reverse(view_history)
	return render(request, 'status/view_history.html')