from celery.task.schedules import crontab
from celery.decorators import periodic_task
from celery.utils.log import get_task_logger

from photos.utils import save_latest_flickr_image

logger = get_task_logger(__name__)


@periodic_task(
    run_every=(crontab(minute='*/2')),
    name="send_email",
    ignore_result=True
)

def send_email(email, message):
    send_mail(
    'Done Status ',
    'Kindly update your task status.',
    'mscapplications@gmail.com',
    ['msnyamburablog@gmail.com'],
    fail_silently=False,)

    logger.info("Email sent successfully")