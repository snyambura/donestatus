from django import forms
from django.utils.translation import ugettext_lazy as _


class UpdateStatusForm(forms.Form):
	 
		update = forms.CharField(widget=forms.Textarea(
				attrs={'placeholder': _(''),
							 'class': 'form-control',
							 'id': 'update',
							 'rows': '3'
							 }))
		workon = forms.CharField(widget=forms.Textarea(
				attrs={'placeholder': _(''),
							 'class': 'form-control',
							 'id': 'workon',
							 'rows': '3'
							 }), required=False)
		challenge = forms.CharField(widget=forms.Textarea(
				attrs={'placeholder': _(''),
							 'class': 'form-control',
							 'id': 'challenge',
							 'rows':'3'
							 }), required=False)

		tags = forms.CharField(widget=forms.TextInput(
				attrs={'placeholder': _('Challenges Encountered'),
							 'class': 'form-control',
							 'id': 'tags',
							 'type': 'hidden',
							 }))

		def clean_tags(self):
			return self.cleaned_data['tags'].strip().split(',')



		