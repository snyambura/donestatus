from django import forms
from django.utils.translation import ugettext_lazy as _
from taggit.models import Tag, TaggedItem
from authentication.models import AppUser


class FilterForm(forms.Form):
	 

		task_date = forms.DateField(widget=forms.TextInput(
		        attrs={'placeholder': _('Date'),
		               'class': 'form-control',
		               'id': 'task_date'
		               }), required=False)

		tags = forms.ModelChoiceField(queryset=Tag.objects.distinct(),
                                       empty_label='Filter by Tags',
                                       widget=forms.Select(
                                           attrs={'class': 'form-control',
                                                  'id': 'tags'
                                                  }),required=False)
		person = forms.ModelChoiceField(queryset=AppUser.objects.distinct(),
                                       empty_label='Filter by Person ',
                                       widget=forms.Select(
                                           attrs={'class': 'form-control',
                                                  'id': 'person'
                                                  }),required=False)




		