
import urlparse
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, JsonResponse
from django.contrib.auth import authenticate, login, logout
from authentication.forms import LoginForm, RegistrationForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.forms import formset_factory
from django.contrib.auth.models import Group
import uuid

from .models import AppUser, RegisterPerson
from .functions import save_temp_data
from django.http import HttpResponse
from django.contrib.auth.models import Permission
from donestatus.views import home 

from django.contrib.auth.views import password_reset_confirm
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.contrib.auth.tokens import default_token_generator
from django.template.response import TemplateResponse
from django.utils.translation import ugettext as _
from django.shortcuts import resolve_url
from django.views.decorators.debug import sensitive_variables


@sensitive_variables('request' 'password')
def log_in(request):
    """Method to handle log in to system."""
    try:
        if request.method == 'POST':
            form = LoginForm(data=request.POST)
            if form.is_valid():
                username = request.POST.get('username').strip()
                password = request.POST.get('password')
                user = authenticate(username=username, password=password)
                if user is not None:
                    if user.is_active:
                        login(request, user)
                        person_id = user.register_person_id
                        names = RegisterPerson.objects.get(pk=person_id)
                        user_names = '%s %s' % (
                            names.first_name, names.last_name)
                        request.session['names'] = user_names
                        next_param = request.GET
                        if 'next' in next_param:
                            next_page = next_param['next']
                            print 'NEXT PAGE', next_page
                            if '/login' not in next_page:
                                return HttpResponseRedirect(next_page)
                        return HttpResponseRedirect(reverse(home))
                    else:
                        msg = "Login Account is currently disabled."
                        messages.add_message(request, messages.ERROR, msg)
                        return render(request, 'login.html', {'form': form})
                else:
                    msg = "Incorrect username and / or password."
                    messages.add_message(request, messages.ERROR, msg)
                    return render(request, 'login.html', {'form': form})
        else:
            form = LoginForm()
            logout(request)
        return render(request, 'login.html', {'form': form, 'status': 200})
    except Exception, e:
        raise e

def log_out(request):
    """Method to handle log out to system."""
    try:
        get_params = request.GET
        user_id = request.user.id
        next_page = '/'
        print "User [%s] successfully logged out." % (request.user.username)
        logout(request)
        msg = 'You have successfully logged out.'
        if 'timeout' in get_params:
            msg = ('You have been logged out due to inactivity. '
                   'Please log in again.')
            messages.add_message(request, messages.ERROR, msg)
        else:
            messages.add_message(request, messages.INFO, msg)
        url = reverse(log_in)
        if 'next' in get_params:
            next_page = get_params['next']
            url = '%s?next=%s' % (url, next_page)
        if 'd' in get_params:
            form_data = get_params['d']
            form_params = dict(urlparse.parse_qsl(form_data))
            # Save this to temp table
            save_temp_data(user_id, next_page, form_params)
            print user_id, next_page, form_params
        return HttpResponseRedirect(url)
    except Exception, e:
        raise e


def register(request):
    """Some default page for the register page."""
    form = RegistrationForm(data=request.POST)
    is_staff = False
    try:
        if request.method == 'POST':
            
            first_name=form.data['first_name'].strip()
            last_name=form.data['last_name'].strip()
            username = form.data['username'].strip()
            password1 = form.data['password1'].strip()
            password2= form.data['password2'].strip()
            email=form.data['email'].strip()
            phone_no=form.data['phone_no'].strip()


            # resolve existing account
            if password1 == password2:
                password = password1
            else:
                msg = 'Passwords do not match!'
                messages.add_message(request, messages.INFO, msg)
                form = RegistrationForm(data=request.POST)
                return render(request, 'register.html',{'form': form},)

            # validate username if__exists
            username_exists = AppUser.objects.filter(username=username)
            if username_exists:
                msg = 'An account with username (%s) already exists.' % username
                messages.add_message(request, messages.INFO, msg)
                form = RegistrationForm(data=request.POST)
                return render(request, 'register.html',{'form': form},)
                if email_exists:
                    msg= 'An account with email (%s) already exixts.' % email
                    messages.add_message(request. messages.INFO, msg)
                    form=RegistrationForm(data=request.POST)
                    return render(request, 'register.html', {'form': form})

            else:
                person = RegisterPerson(
                first_name=first_name,
                last_name=last_name,
                phone_no=phone_no,
                email=email,
                is_void=False)

                person.save()

                register_person_pk = int(person.pk)

                # Create User
                user = AppUser.objects.create_user( password=password,
                                                    username=username,
                                                    is_staff=False,
                                                    register_person=register_person_pk, 
                                                   )
                if user:

                    msg = 'User (%s) save success.' % (username)
                    messages.add_message(request, messages.INFO, msg)
                    form=LoginForm()
                    redirect_url = reverse(log_in)
                    return HttpResponseRedirect(redirect_url)

            register_person_pk = person.pk
            now = timezone.now()
        else:
            form = RegistrationForm()
            return render(request, 'register.html',
                          {'form': form},)
    except Exception, e:
        msg = 'Error - (%s) ' % (str(e))
        messages.add_message(request, messages.ERROR, msg)
        return render(request, 'register.html',
                              {'form': form},)


