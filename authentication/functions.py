"""Common functions for authentication module."""
from django.utils import timezone


def save_temp_data(user_id, page_id, page_data):
    """"Method to save temp form data for this person and page."""
    try:
        new_tmp, ctd = RegTemp.objects.update_or_create(
            user_id=user_id, page_id=page_id,
            defaults={'data': str(page_data), 'created_at': timezone.now(),
                      'user_id': user_id, 'page_id': page_id},)
    except Exception, e:
        print 'save tmp error - %s' % (str(e))
        pass

