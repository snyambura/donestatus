"""URLs for authentication module."""
from django.conf.urls import url
from .views import log_in, register, home

# This should contain urls related to auth app ONLY
urlpatterns = [
               url(r'^register/$',register, name='register'),
               url(r'^login/$',log_in, name='login'),
               ]
