"""Forms for authentication module."""
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth import get_user_model
from django.template import loader
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from .models import AppUser



class RegistrationForm(forms.Form):
    """Registration form."""

    first_name = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('First name'),
               'class': 'form-control',
               'data-parsley-required': "true",
               'autofocus': 'true'}))
    last_name = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Last name'),
               'class': 'form-control',
               'data-parsley-required': "true",
               'autofocus': 'true'}))
    username = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Username'),
               'class': 'form-control',
               'data-parsley-required': "true",
               'autofocus': 'true'}))
    password1 = forms.CharField(widget=forms.PasswordInput(
        attrs={'placeholder': _('Password'),
               'class': 'form-control',
               'data-parsley-required': "true",
               'autofocus': 'true'}))
    password2 = forms.CharField(widget=forms.PasswordInput(
        attrs={'placeholder': _('Re-enter password'),
               'class': 'form-control',
               'data-parsley-required': "true",
               'autofocus': 'true'}))
    phone_no = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Phone number'),
               'class': 'form-control',
               'data-parsley-required': "true",
               'autofocus': 'true'}))
    email = forms.CharField(widget=forms.EmailInput(
        attrs={'placeholder': _('Email Address'),
               'class': 'form-control',
               'id': 'email',
               'data-parsley-required': "true",
               'data-parsley-type': 'email'})) 


    def clean_username(self):
        """Method to clean username."""
        try:
            AppUser.objects.get(username__iexact=self.cleaned_data['username'])
        except AppUser.DoesNotExist:
            return self.cleaned_data['username']
        raise forms.ValidationError(_(
            "The username already exists. Please try another one."))

    def clean(self):
        """Method to compare passwords."""
        form_obj = self.cleaned_data
        if 'password1' in form_obj and 'password2' in form_obj:
            if form_obj['password1'] != form_obj['password2']:
                raise forms.ValidationError(
                    _("The two password fields did not match."))
        return self.cleaned_data


class LoginForm(forms.Form):
    """Login form for the application."""

    username = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Username'),
               'class': 'form-control input-lg',
               'data-parsley-required': "true",
               'data-parsley-error-message': "Please enter your username.",
               'autofocus': 'true'}),
        error_messages={'required': 'Please enter your username.',
                        'invalid': 'Please enter a valid username.'})
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={'placeholder': _('Password'),
               'class': 'form-control input-lg',
               'data-parsley-required': "true",
               'data-parsley-error-message': "Please enter your password.",
               'autofocus': 'true'}),
        error_messages={'required': 'Please enter your password.',
                        'invalid': 'Please enter a valid password.'},)

    def clean_username(self):
        """Method to clean username."""
        username = self.cleaned_data['username']
        if not username:
            raise forms.ValidationError("Please enter your username.")
        return username

    def clean_password(self):
        """Method to clean password."""
        password = self.cleaned_data['password']
        if not password:
            raise forms.ValidationError("Please enter your password.")
        return password


