from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.core.mail import send_mail
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin, Group, Permission)
from datetime import datetime


class CPOVCUserManager(BaseUserManager):

    def create_user(self, username, register_person, is_staff=False, password=None):

        from .models import RegisterPerson
        if not username:
            raise ValueError('The given username must be set')

        now = datetime.now()
        user = self.model(username=username,
                          register_person=RegisterPerson.objects.get(pk=register_person),
                          password=password,
                          is_staff=False,
                          is_active=True,
                          is_superuser=False,
                          role='Public',
                          timestamp_created=now,
                          timestamp_updated=now,
                          )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, register_person, password=None):
        user = self.create_user(username=username,
                                register_person=register_person,
                                password=password
                                )
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class AppUser(AbstractBaseUser, PermissionsMixin):
    register_person = models.OneToOneField('authentication.RegisterPerson', null=False)
    role = models.CharField(max_length=20, unique=False, default='Public')
    username = models.CharField(max_length=20, unique=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)
    timestamp_created = models.DateTimeField(auto_now_add=True)
    timestamp_updated = models.DateTimeField(auto_now=True)
    password_changed_timestamp = models.DateTimeField(null=True)

    # For admin so remove if you don't want to use admin
    first_name = False
    last_name = False
    date_joined = False

    objects = CPOVCUserManager()
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['register_person']

    def _get_email(self):
        return self.register_person.email

    email = property(_get_email)

    def _get_first_name(self):
        return self.register_person.first_name

    first_name = property(_get_first_name)

    def _get_surname(self):
        return self.register_person.surname

    surname = property(_get_surname)

    # def _get_last_name(self):
    #     return self.register_person.other_names

    # last_name = property(_get_last_name)

    def get_full_name(self):
        """
        TO DO - Get this from persons table but for now just use
        Workforce ID
        """
        return self.username

    def get_short_name(self):
        """
        Return Workforce ID if exists or not
        """
        return self.username

    def get_username(self):
        """
        Return National ID if exists else Workforce ID
        """
        if self.username:
            return self.username
        else:
            return None

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        db_table = 'auth_user'

class RegisterPerson(models.Model):
    """Model for Persons details."""
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255, default=None)
    email = models.EmailField(blank=True, default=None)
    phone_no = models.CharField(null=True, blank=True, default=None, max_length=13)

    is_void = models.BooleanField(default=False)
    # created_by = models.ForeignKey(AppUser, null=True)
    created_at = models.DateField(default=timezone.now)

    def _get_persons_data(self):
        _register_persons_data = RegisterPerson.objects.all().order_by('-id')
        return _register_persons_data

    def _get_full_name(self):
        return '%s %s %s' % (self.first_name, self.last_name)

    def make_void(self):
        """Inline call method."""
        self.is_void = True
        super(RegisterPerson, self).save()



    full_name = property(_get_full_name)


    class Meta:
        """Override table details."""

        db_table = 'register_person'
        verbose_name = 'Persons Registration'
        verbose_name_plural = 'Persons Registrations'

    def __unicode__(self):
        """To be returned by admin actions."""
        return '%s %s %s' % (self.first_name, self.last_name)



